import { Component, OnInit } from '@angular/core';
import data from '../../../data/data.json'

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  ticket_masuk:any;
  activity:any;
  slides = [
    {img: "http://placehold.it/350x150/000000"},
    {img: "http://placehold.it/350x150/111111"},
    {img: "http://placehold.it/350x150/333333"},
    {img: "http://placehold.it/350x150/666666"}
  ];
  slideConfig = {"slidesToShow": 3, "slidesToScroll": 4};
  constructor() { 
    this.ticket_masuk = data.ticket_masuk
    this.activity = data.activity
  }

  ngOnInit(): void {
  }

  formatNumber(number){
    const format = number.toString().split('').reverse().join('');
    const convert = format.match(/\d{1,3}/g);
    const num = convert.join('.').split('').reverse().join('')
      // return number.toString().replace(/\B(?<!\.\d*)(?=(\d{3})+(?!\d))/g, ",");
      return num;
  }

  

}
