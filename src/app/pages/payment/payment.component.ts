import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router'
import {Store} from '@ngrx/store'
import {loadCart, deleteCart} from '../../actions/cart.actions'

@Component({
  selector: 'app-payment',
  templateUrl: './payment.component.html',
  styleUrls: ['./payment.component.scss']
})
export class PaymentComponent implements OnInit {
  grandtotal:number
  cart:any
  ref_number:any
  path:any

  constructor(private store:Store<any>,private router:Router) { 
    this.path = router.url
    var convert = this.path.split('/')
    this.ref_number = convert[3]
  }
  
  ngOnInit(): void {
    
    this.store.dispatch(new loadCart())
    this.store.subscribe(state=>(this.cart=state.cart.cart))
    this.grandtotal = this.grandTotal(this.cart)
  }

  continuePayment(){
    this.router.navigate(['/payment/success'])
  }

  formatNumber(number){
    const format = number.toString().split('').reverse().join('');
    const convert = format.match(/\d{1,3}/g);
    const num = convert.join('.').split('').reverse().join('')
      return num;
  }

  grandTotal(arr) {
    return arr.reduce((sum, i) => {
      return sum + (i.originalPrice * i.quantity)
    }, 0)
  };

}
