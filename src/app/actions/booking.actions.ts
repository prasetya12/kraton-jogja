import {Action} from '@ngrx/store'

export enum BookingActionTypes {
    CREATE_BOOKING = '[BOOKING] CREATE_BOOKING',
    CREATE_BOOKING_SUCCESS = '[BOOKING] CREATE_BOOKING_SUCCESS',
    CREATE_BOOKING_FAILURE = '[BOOKING] CREATE_BOOKING_FAILURE',
}

export class createBooking implements Action{
    readonly type = BookingActionTypes.CREATE_BOOKING
    constructor(public payload:any=null){}
  
  }
  
  export class createBookingSuccess implements Action{
    readonly type = BookingActionTypes.CREATE_BOOKING_SUCCESS
    constructor(public payload:any=null){}
  }
  
  export class createBookingFailure implements Action{
    readonly type = BookingActionTypes.CREATE_BOOKING_FAILURE
    constructor(public payload:any=null){ }
  }