import {Action} from '@ngrx/store'

export class loadCart implements Action{
  readonly type = "LOAD_CART"
}

export class addCart implements Action{
  readonly type = "ADD_CART"
  constructor(public payload:any){}
}

export class deleteCart implements Action{
  readonly type = "DELETE_CART"
  constructor(public payload:number){}
}

export class plusCart implements Action{
  readonly type = "PLUS_CART"
  constructor(public payload:number){

  }
}

export class minusCart implements Action{
  readonly type = "MINUS_CART"
  constructor(public payload:number){}
}

  export type Actions = loadCart | addCart | deleteCart | plusCart | minusCart