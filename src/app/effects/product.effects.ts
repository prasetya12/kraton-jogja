import {Injectable} from '@angular/core'
import {Actions, createEffect, ofType,concatLatestFrom} from '@ngrx/effects'
import {of} from 'rxjs'
import {map, exhaustMap, catchError,switchMap,mergeMap} from 'rxjs/operators'
import {ProductService} from '../services/product.service'
import {select, Store} from '@ngrx/store'
import * as productsAction from '../actions/product.actions'


@Injectable()
export class productEffects{
    constructor(
        private action$ : Actions,
        private productService : ProductService,
        private store:Store<any>
    ){}

    getAttractions = createEffect(()=>
        this.action$.pipe(
            ofType(productsAction.AttractionActionTypes.GET_ATTRACTIONS),
            switchMap((action:any)=>
                this.productService.getAttractions(action.payload).pipe(
                    map((res:any)=>{
                        return new productsAction.getAttractionsSuccess(res.data)
                    }),
                    catchError((error:any)=>of(new productsAction.getAttractionsFailure(error)))
                ))
        ))
    getActivities = createEffect(()=>
        this.action$.pipe(
            ofType(productsAction.AttractionActionTypes.GET_ACTIVITIES),
            switchMap((action:any)=>
                this.productService.getAttractions(action.payload).pipe(
                    map((res:any)=>{
                        return new productsAction.getActivitiesSuccess(res.data)
                    }),
                    catchError((error:any)=>of(new productsAction.getActivitiesFailure(error)))
                ))
        ))
    getTours = createEffect(()=>
        this.action$.pipe(
            ofType(productsAction.AttractionActionTypes.GET_TOURS),
            switchMap((action:any)=>
                this.productService.getAttractions(action.payload).pipe(
                    map((res:any)=>{
                        return new productsAction.getToursSuccess(res.data)
                    }),
                    catchError((error:any)=>of(new productsAction.getToursFailure(error)))
                ))
        ))

    getDetailAttraction = createEffect(()=>
        this.action$.pipe(
            ofType(productsAction.AttractionActionTypes.GET_DETAIL_ATTRACTION),
            switchMap((action:any)=>
                this.productService.getDetailAttraction(action.payload).pipe(
                    map((res:any)=>{
                        return new productsAction.getDetailAttractionSuccess(res.data)
                    }),
                    catchError((error:any)=>of(new productsAction.getDetailAttractionFailure(error)))
                ))
        ))

    getTicketType = createEffect(()=>
        this.action$.pipe(
            ofType(productsAction.AttractionActionTypes.GET_TICKET_TYPE),
            switchMap((action:any)=>
                this.productService.getTicketType(action.payload).pipe(
                    map((res:any)=>{
                        return new productsAction.getTicketTypeSuccess(res.data)
                    }),
                    catchError((error:any)=>of(new productsAction.getTicketTypeFailure(error)))
                ))
        ))
}