import { Injectable } from '@angular/core';
import {API} from '../../constants/urls';
import { HttpClient, HttpHeaders ,HttpParams } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class BookingService {

  constructor(private http: HttpClient) { }

  createBooking(data){
    return this.http.post(API.CREATE_BOOKING, data)
  }
}
