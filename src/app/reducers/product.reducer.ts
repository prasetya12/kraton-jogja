import {createSelector} from '@ngrx/store'
import data from '../../data/data.json'
import {AttractionActionTypes} from '../actions/product.actions'

const initialState={
    product:data,
    detailProduct:{},
    data:[],
    activities:[],
    tours:[],
    dataTicketType:[],
    isLoding:false,
    isError:false,
    errorMessage:{}
}

export function productReducer(state=initialState,action){
    switch(action.type){
        case AttractionActionTypes.GET_ATTRACTIONS:
            return {
                ...state,
                isLoading:true,
                isError:false
            }
        case AttractionActionTypes.GET_ATTRACTIONS_SUCCESS:
            return {
                ...state,
                isLoading:false,
                isError:false,
                data:action.payload
            }
        case AttractionActionTypes.GET_ATTRACTIONS_FAILURE:
            return{
                ...state,
                isLoading:false,
                isError:true,
                errorMessage:action.payload
            }
        case AttractionActionTypes.GET_ACTIVITIES:
            return {
                ...state,
                isLoading:true,
                isError:false
            }
        case AttractionActionTypes.GET_ACTIVITIES_SUCCESS:
            return {
                ...state,
                isLoading:false,
                isError:false,
                activities:action.payload
            }
        case AttractionActionTypes.GET_ACTIVITIES_FAILURE:
            return{
                ...state,
                isLoading:false,
                isError:true,
                errorMessage:action.payload
            }
        case AttractionActionTypes.GET_TOURS:
            return {
                ...state,
                isLoading:true,
                isError:false
            }
        case AttractionActionTypes.GET_TOURS_SUCCESS:
            return {
                ...state,
                isLoading:false,
                isError:false,
                tours:action.payload
            }
        case AttractionActionTypes.GET_TOURS_FAILURE:
            return{
                ...state,
                isLoading:false,
                isError:true,
                errorMessage:action.payload
            }
        case AttractionActionTypes.GET_DETAIL_ATTRACTION:
            return {
                ...state,
                isLoading:true,
                isError:false
            }
        case AttractionActionTypes.GET_DETAIL_ATTRACTION_SUCCESS:
            return {
                ...state,
                isLoading:false,
                isError:false,
                detailProduct:action.payload
            }
        case AttractionActionTypes.GET_DETAIL_ATTRACTION_FAILURE:
            return{
                ...state,
                isLoading:false,
                isError:true,
                errorMessage:action.payload
            }
        case AttractionActionTypes.GET_TICKET_TYPE:
            return {
                ...state,
                isLoading:true,
                isError:false
            }
        case AttractionActionTypes.GET_TICKET_TYPE_SUCCESS:
            return {
                ...state,
                isLoading:false,
                isError:false,
                dataTicketType:action.payload
            }
        case AttractionActionTypes.GET_TICKET_TYPE_FAILURE:
            return{
                ...state,
                isLoading:false,
                isError:true,
                errorMessage:action.payload
            }
        case AttractionActionTypes.REMOVE_DATA:
            return{
                ...state,
                data:[]
            }
        default:
            return state
    }
}

export const selectProductState = (state) => state.product;
export const selectProduct = createSelector(selectProductState, (state) => state.product);