import { Component, OnInit,HostListener } from '@angular/core';
import {HashLocationStrategy, Location, LocationStrategy} from '@angular/common';
import {Subscription} from 'rxjs';
import {loadCart} from '../../actions/cart.actions'
import {select,Store} from '@ngrx/store'


@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  cart:any
  path:any
  place:string
  subscription:Subscription
  notifCart:Number

  constructor(location: Location,private store:Store<any>) { 
    this.store.dispatch(new loadCart())
    const temp_cart =  this.store.pipe(select(state => state.cart.cart,{id:'1'}))
    temp_cart.subscribe(res => {

        this.cart = res
        let total =0
        res.forEach((item)=>{
          total +=item.quantity
        })
        this.notifCart=total

      });
    location.onUrlChange((url, state) => {
      this.path = url.split('/')
      this.place=this.path[1]


    });

  }

  ngOnInit(): void {

  }

  toAbout() {
    document.getElementById("about").scrollIntoView()  
  }

  toTicket() {
    document.getElementById("ticket").scrollIntoView()  
  }

  toActivity() {
    document.getElementById("activity").scrollIntoView()  
  }


}
